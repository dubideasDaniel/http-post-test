import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

class HttpPostTest {
  public static void main(String[] args) throws UnirestException {

    Unirest.setTimeouts(0, 0);
    HttpResponse<String> response = Unirest.post("https://woo.charrua.es/wc-api/wc_gateway_crandon")
        .header("Content-Type", "application/x-www-form-urlencoded")
        .field("pedido_id", "13")
        .field("estado_pago", "1")
        .field("respuesta_pago", "Pago correcto referencia 123456789")
        .field("cliente_id", "hola@charrua.es")
        .field("total", "30000")
        .field("sb_token", "132456789untokendesistarbanc")
        .field("sec_token", "dQDGZtMeXR2HrOeTmKrZ9qUovH95eoYM")
        .asString();

        System.out.println(response.getBody());
        System.out.println(response.getStatus());
  }
}